import { Heading, Grid } from '@chakra-ui/react'
import ProductCard from '../components/ProductCard'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { listMensProducts} from '../actions/productActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
const MenScreen = () => {

	const dispatch = useDispatch();

	const productMensList = useSelector((state) => state.productMensList);
	const { loading, error, products } = productMensList;
	console.log(productMensList);
	useEffect(() => {
		dispatch(listMensProducts());
	}, [dispatch]);
	return (
		<>
			<Heading as='h2' mb='8' fontSize='xl'>
				Mens Collection
			</Heading>

			{loading ? (
				<Loader/>
			) : error ? (
				<Message type='error' >{error}</Message>
			) : (
				<Grid templateColumns={{base:'1fr',md:'1fr 1fr 1fr 1fr'}} gap='8'>
					{products.map((prod) => (
						<ProductCard key={prod._id} product={prod} />
					))}
				</Grid>
			)}
		</>
	)
}

export default MenScreen