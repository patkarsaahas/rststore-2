import { Flex, flex } from '@chakra-ui/react';

const FormcContainer = ({children }) => {
    return (
        <Flex direction='column' boxShadow='md' rounded='md' bgColor='white' p='10' width={{base:'100%',md:'xl'}}>
            {children}
        </Flex>
    );
};

export default FormcContainer